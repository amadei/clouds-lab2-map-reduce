import logging
import os
import azure.functions as func
import azure.durable_functions as df
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient


app = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

ACCOUNT_URL = 'https://lab2mapreduce.blob.core.windows.net'
CONTAINER_NAME = 'mrinput'


@app.route(route='word_count')
@app.durable_client_input(client_name='client')
async def http_start(req: func.HttpRequest, client: df.DurableOrchestrationClient):
    instance_id = await client.start_new('orchestrator')
    response = client.create_check_status_response(req, instance_id)
    return response


@app.orchestration_trigger(context_name='context')
def orchestrator(context: df.DurableOrchestrationContext):
    lines = yield context.call_activity('get_input_data', ACCOUNT_URL)

    tasks = []
    for i, line in enumerate(lines):
        tasks.append(context.call_activity('mapper', (i, line)))
    word_list = yield context.task_all(tasks)

    if word_list is None:
        word_list = []

    flat_word_list = [j for i in word_list for j in i]

    words = yield context.call_activity("shuffler", flat_word_list)

    tasks = []
    for word in words:
        tasks.append(context.call_activity('reducer', word))
    word_cont = yield context.task_all(tasks)

    return word_cont


@app.activity_trigger(input_name='url')
def get_input_data(url: str) -> list[str]:
    try:
        # default_credential = DefaultAzureCredential()
        # blob_service_client = BlobServiceClient(url, credential=default_credential)

        blob_service_client = BlobServiceClient.from_connection_string(os.getenv('AZURE_STORAGE_CONNECTION_STRING'))

        container_client = blob_service_client.get_container_client(CONTAINER_NAME)
        blob_list = container_client.list_blobs()

        all_lines = []
        for blob in [i.name for i in blob_list]:
            blob_client = blob_service_client.get_blob_client(container=CONTAINER_NAME, blob=blob)
            downloader = blob_client.download_blob(max_concurrency=1, encoding='UTF-8')
            blob_lines = downloader.readall().replace('\r', '').split('\n')

            all_lines.extend(blob_lines)

        return all_lines
    
    except Exception as e:
        logging.error(e)
        return []


@app.activity_trigger(input_name='line')
def mapper(line: tuple[int, str]) -> list[tuple[str, int]]:
    result = []
    words = line[1].strip().lower().replace('.', '').replace(',', '').split()

    for w in words:
        result.append((w, 1))
    
    return result


@app.activity_trigger(input_name='word')
def reducer(word: tuple[str, list[int]]) -> tuple[str, int]:
    return (word[0], sum(word[1]))


@app.activity_trigger(input_name='words')
def shuffler(words: list[tuple[str, int]]) -> list[tuple[str, list[int]]]:
    word_dict: [str, list[int]] = {}
    
    for w, n in words:
        if w in word_dict:
            word_dict[w].append(n)
        else:
            word_dict[w] = [n]

    return list(word_dict.items())
